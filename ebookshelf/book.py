import xml.etree.cElementTree as SVG
from math import sqrt
from itertools import groupby
import random
import hashlib


class Book:

    maxPages = 0

    def xml2dict(self,e):
        """Convert an etree into a dict structure

        @type  e: etree.Element
        @param e: the root of the tree
        @return: The dictionary representation of the XML tree
        """
        def _xml2dict(e):
            children = dict(e.attrib)
            if e.text.strip():
                children['_text_'] = e.text.strip()
            if e.tail.strip():
                children['_tail_'] = e.tail.strip()
            for k, g in groupby(e, lambda x: x.tag):
                g = [ _xml2dict(x) for x in g ] 
                children[k]=  g
            return children

        return { e.tag : _xml2dict(e) }

    def attribute(self, att):
        if att in["_seiten","_worte","size"]:     # integer attributes
            if self.dictstore["record"].get(att):
                return int(self.dictstore["record"][att][0]["_text_"],0)
            else:
                return 0
        elif att in["tags","formats","authors"]:  # list attributes
            if self.dictstore["record"].get(att):
                result = []
                for element in self.dictstore["record"][att][0][att[0:-1]]:
                    result.append(element["_text_"])
                return result
            else:
                return []
        else:
            if self.dictstore["record"].get(att):         # string attributes
                return self.dictstore["record"][att][0]["_text_"]
            else:
                return"None"
                

    def __init__(self, booknode):
        self.dictstore = self.xml2dict(booknode)
        if self.attribute("_seiten") > Book.maxPages:
            Book.maxPages = self.attribute("_seiten")

    def shelfspace(self):
        return sqrt(1-(1-(self.attribute("_seiten") / Book.maxPages) ** 2)) * 90 + 10

    def draw(self, svg, bottom, position):
        hex = hashlib.md5(self.attribute("publisher").encode('utf-8')).hexdigest()
        h = int(int("0x" + str(hex)[-2:], 16)/2)
        col = "#" + str(hex)[-6:]
        pubcol = "#" + str(hex)[-8:-2]
        hex = hashlib.md5(self.attribute("series").encode('utf-8')).hexdigest()
        sercol = "#" + str(hex)[-8:-2]

        link = SVG.SubElement(svg, "a",
                              href=self.attribute("formats")[0],
                              type="application/epub+zip")

        linktitle = SVG.SubElement(link, "title")
        linktitle.text = self.attribute("title")

        book = SVG.SubElement(link, "rect",
                              fill=col,
                              stroke="black",
                              x=str(position),
                              y=str(bottom-192-h),
                              width=str(self.shelfspace()),
                              height=str(192+h))
        book.set("stroke-width", "0.5")

        publisher = SVG.SubElement(link, "rect",
                                   fill=pubcol,
                                   stroke="black",
                                   x=str(position),
                                   y=str(bottom-16),
                                   width=str(self.shelfspace()),
                                   height=str(16))
        publisher.set("stroke-width", "0.5")

        series = SVG.SubElement(link, "rect",
                                fill=sercol,
                                stroke="black",
                                x=str(position),
                                y=str(bottom - 192 - h),
                                width=str(self.shelfspace()),
                                height=str(16))
        series.set("stroke-width", "0.5")

        title = SVG.SubElement(link, "text",
                               x=str(position + self.shelfspace()/2 + 4),
                               y=str(bottom - 170 - h))
        title.set("stroke-width", "0.5")
        title.set("transform",
                  "rotate(-90 " + str(position+self.shelfspace()/2 + 4) +
                  " " + str(bottom - 170 - h) + ")")
        title.set("stroke", "white")
        title.set("text-anchor", "end")
        title.set("font-family", "Arial")
        title.set("font-size", "10")
        title.set("fill", "white")
        title.text = self.attribute("title")[:30]

        author = SVG.SubElement(link, "text",
                                x=str(position+self.shelfspace()/2 + 4),
                                y=str(bottom-16))
        author.set("stroke-width", "0.5")
        author.set("transform",
                   "rotate(-90 " + str(position+self.shelfspace()/2 + 4) +
                   " " + str(bottom-16)+")")
        author.set("stroke", "white")
        author.set("text-anchor", "start")
        author.set("font-family", "Arial")
        author.set("font-size", "5")
        author.set("fill", "white")
        author.text = self.attribute("authors")[0][:30]
